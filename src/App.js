import React from 'react';
import groupBy from "lodash/groupBy";
import Header from './components/Header/Header';
import Filter from './components/Filter/Filter';
import Chart from './components/Chart/Chart';
import Pagination from './components/Pagination/Pagination';
import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullData: {},
      data: false,
      filterDays: false,
      order: true,
      currentPage: 0,
      chunkSize: 30
    };
  }

  createChart = (data, filter) => {
    const state = this.state;
    let filterLength = filter ? filter : false;
    let chartData = [];
    let chartDataChunks = [];
    Object.keys(data).reverse().forEach(day => {
      chartData.push({
        day: day,
        trees: data[day].reduce((all, current) => all + current.value, 0)
      });
    });
    if(filterLength) {
      chartData = chartData.slice(0, filterLength);
      console.log(filterLength);
    }
    for(var i = 0; i < chartData.length; i += state.chunkSize) {
      chartDataChunks.push(chartData.slice(i, i+state.chunkSize));
    }
    this.setState({
      ...state,
      data: chartData,
      dataChunks: chartDataChunks,
      fullData: data,
      filterDays: filterLength,
    })
  }

  componentDidMount() {
    fetch("https://public.offset.earth/trees")
      .then(res => res.json())
      .then(
        (result) => {
          let grouped = groupBy(result, res => res.createdAt.split("T")[0]);
          this.createChart(grouped);
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  
  changePage(newPage) {
    this.setState({
      currentPage: newPage,
    })
  }

  changeOrder(order) {
    const state = this.state;
    let orderState = order === 'start' ? true : false;
    if(state.order !== orderState) {
      let reOrderedData = [...state.data].reverse();
      let orderedChunk = [];
      for(var i = 0; i < reOrderedData.length; i += state.chunkSize) {
        orderedChunk.push(reOrderedData.slice(i, i+state.chunkSize));
      }
      this.setState({
          ...state,
          data: reOrderedData,
          dataChunks: orderedChunk,
          order: orderState
      })
    }    
  }

  changeLength(length) {
    this.createChart(this.state.fullData, length)
  }

  render({order, filterDays, currentPage, dataChunks, data} = this.state) {
    if (!data) {
      return null
    } 
    

    return (
      <div id="App">
      <Header />
      <Filter order={order} filterDays={filterDays} onChangeOrder={this.changeOrder.bind(this)} onChangeLength={this.changeLength.bind(this)} />
      <Chart data={dataChunks[currentPage]}/>
      <Pagination onClick={this.changePage.bind(this)} dataChunks={dataChunks} currentPage={currentPage}/>
      </div>
    );
  }
}

export default App;
