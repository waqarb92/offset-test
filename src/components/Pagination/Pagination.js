import React from 'react';
import propTypes from 'prop-types';
import './Pagination.scss'


const Pagination = ({dataChunks, onClick, currentPage}) => {
    return (
        <div className="Pagination">
        <h2>Maximum 30 items per page:</h2>
        <div className="Pagination__links">
          {dataChunks.map((page, index) => {
              return (<button className={currentPage === index ? 'active' : null} key={index} type="button" onClick={() => onClick(index)}>{index}</button>);
            })}
        </div>
      </div>
    )
}

Pagination.propTypes = {
    dataChunks: propTypes.array.isRequired,
    currentPage: propTypes.number.isRequired,
    onClick: propTypes.func.isRequired,
}

export default Pagination;
