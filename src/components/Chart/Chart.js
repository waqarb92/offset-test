import React from 'react';
import propTypes from 'prop-types';
import { VictoryBar, VictoryChart, VictoryAxis } from 'victory';
import "./Chart.scss"


const Chart = ({data}) => {
    return (
        <div className="Chart">
            <VictoryChart domainPadding={20}>
                <VictoryAxis
                    style={{
                    axis: {stroke: "#756f6a"},
                    axisLabel: {fontSize: 5, padding: 30},
                    ticks: {stroke: "grey", size: 10},
                    tickLabels: {fontSize: 5, paddingTop: 20, angle:75, verticalAnchor: "middle", textAnchor:'start'}
                    }}
                />
                <VictoryAxis
                    dependentAxis
                    style={{
                    axis: {stroke: "#756f6a"},
                    axisLabel: {fontSize: 5, padding: 30},
                    ticks: {stroke: "grey", size: 10},
                    tickLabels: {fontSize: 5, padding: 20, verticalAnchor: "middle", textAnchor:'start'}
                    }}
                />
                <VictoryBar
                    data={data}
                    x="day"
                    y="trees"
                />
            </VictoryChart>
        </div>
    )
}

Chart.propTypes = {
    data: propTypes.array.isRequired,
}

export default Chart;
