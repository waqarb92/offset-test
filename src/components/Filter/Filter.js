import React from 'react';
import propTypes from 'prop-types';
import './Filter.scss'


const Filter = ({order, filterDays, onChangeOrder, onChangeLength}) => {

    const handleChangeOrder = (order) => {
        onChangeOrder(order)
    }

    return (
        <div className="Filter">
        <div className="Order">
        <label for="start" className={order ? 'active' : null}>
          <input type="radio" id="start" value="start" onChange={() => handleChangeOrder('start')} name="order-filter" checked={order ? true : false}/>
          From the first
        </label>
        <label for="current" className={!order ? 'active' : null}>
          <input type="radio" id="current" value="current" onChange={() => handleChangeOrder('current')} name="order-filter" checked={!order ? true : false}/>
          From the newest
        </label>
        </div>
        <div className="Length">
        <label for="thirty" className={filterDays === 30 ? 'active' : null}>
          <input type="radio" id="thirty" value="thirty" onChange={() => onChangeLength(30)} name="Length-filter" checked={filterDays === 30 ? true : false}/>
          30 Days
        </label>
        <label for="sixty" className={filterDays === 60 ? 'active' : null}>
          <input type="radio" id="sixty" value="sixty" onChange={() => onChangeLength(60)} name="Length-filter" checked={filterDays === 60 ? true : false}/>
          60 Days
        </label>
        <label for="all" className={!filterDays ? 'active' : null}>
          <input type="radio" id="all" value="all" onChange={() => onChangeLength(false)} name="Length-filter" checked={!filterDays ? true : false}/>
          All time
        </label>
        </div>
        </div>
    )
}

Filter.propTypes = {
    order: propTypes.bool.isRequired,
    filterDays: propTypes.oneOfType([
        propTypes.number,
        propTypes.bool
    ]).isRequired,
    onChangeOrder: propTypes.func.isRequired,
    onChangeLength: propTypes.func.isRequired,
}

export default Filter;


