import React from 'react';
import './Header.scss'


const Header = () => {
    return (
        <div className="Header">
            <div className="Header__wrap">
            <h1>Trees Planted Since Launch</h1>
            <h2>Front end programming test using Ecologi trees api</h2>
            </div>
        </div>
    )
}


export default Header;


