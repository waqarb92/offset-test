## Task Description
Basic styling completed
No Mobile styling done
Basic filter features (ordering, date periods)
Pagination added to chart to better display data

Please run:

### `npm install`

Followed by

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


### API Recommendations
Data organised into trees per day<br />
API to request specific date ranges
